<?php

namespace Drupal\ai_interpolator_dreamstudio;

use Drupal\ai_interpolator\Exceptions\AiInterpolatorRequestErrorException;
use Drupal\ai_interpolator_dreamstudio\Form\DreamStudioConfigForm;
use Drupal\Core\Config\ConfigFactory;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;

/**
 * Dream Studio API creator.
 */
class DreamStudio {

  /**
   * The http client.
   */
  protected Client $client;

  /**
   * API Key.
   */
  private string $apiKey;

  /**
   * The base path.
   */
  private string $basePath = 'https://api.stability.ai/';

  /**
   * Constructs a new DreamStudio object.
   *
   * @param \GuzzleHttp\Client $client
   *   Http client.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   */
  public function __construct(Client $client, ConfigFactory $configFactory) {
    $this->client = $client;
    $this->apiKey = $configFactory->get(DreamStudioConfigForm::CONFIG_NAME)->get('api_key') ?? '';
  }

  /**
   * Get Engines.
   *
   * @return array
   *   Engine key and name.
   */
  public function engines() {
    return json_decode($this->makeRequest("v1/engines/list", [], 'GET')->getContents(), TRUE);
  }

  /**
   * Create image from text.
   *
   * @param string $prompt
   *   The prompt to use.
   * @param string $style_preset
   *   The style preset.
   * @param string $engine_id
   *   Dream Studios Engine.
   * @param int $width
   *   The width.
   * @param int $height
   *   The height.
   * @param int $steps
   *   The amount of steps.
   * @param array $options
   *   Other options to add.
   *
   * @return string
   *   Binary.
   */
  public function generateImage($prompt, $style_preset = 'photographic', $engine_id = 'stable-diffusion-xl-beta-v2-2-2', $width = 512, $height = 512, $steps = 150, $options = []) {
    $data = $options;
    $data['height'] = (int) $height;
    $data['width'] = (int) $width;
    $data['style_preset'] = $style_preset;
    $data['text_prompts'][] = [
      'text' => $prompt,
      'weight' => 1,
    ];
    $data['steps'] = (int) $steps;
    $guzzleOptions['headers']['accept'] = 'image/png';
    $guzzleOptions['headers']['Content-Type'] = 'application/json';

    return $this->makeRequest("v1/generation/$engine_id/text-to-image", [], 'POST', json_encode($data), $guzzleOptions)->getContents();
  }

  /**
   * Upscale image.
   *
   * @param string $binary
   *   The binary.
   * @param int $width
   *   The width.
   * @param int $height
   *   The height.
   * @param string $engine
   *   The engine.
   *
   * @return string
   *   Binary.
   */
  public function upscaleImage($binary, $width = 0, $height = 0, $engine = 'esrgan-v1-x2plus') {
    $extra = $width ? ['name' => 'width', 'contents' => $width] : ['name' => 'height', 'contents' => $height];
    $guzzleOptions['headers']['accept'] = 'image/png';
    $guzzleOptions['multipart'] = [
      [
        'name' => 'image',
        'contents' => $binary,
      ],
      $extra,
    ];

    return $this->makeRequest("v1/generation/$engine/image-to-image/upscale", [], 'POST', "", $guzzleOptions)->getContents();
  }

  /**
   * Search and replace.
   *
   * @param string $binary
   *   The binary.
   * @param string $prompt
   *   The prompt to use.
   * @param string $searchPrompt
   *   The search prompt.
   * @param string $negativePrompt
   *   The negative prompt.
   * @param string $outputFormat
   *   The return format.
   *
   * @return string
   *   Binary.
   */
  public function searchAndReplace($binary, $prompt, $searchPrompt, $negativePrompt = '', $outputFormat = 'png') {
    // Check so return format is one of png, jpg or webp.
    if (!in_array($outputFormat, ['png', 'jpg', 'webp'])) {
      throw new AiInterpolatorRequestErrorException('The return format has to be one of png, jpg or webp.');
    }
    $guzzleOptions['multipart'] = [
      [
        'name' => 'image',
        'contents' => $binary,
        'filename' => 'image.' . $outputFormat,
      ],
      [
        'name' => 'prompt',
        'contents' => $prompt,
      ],
      [
        'name' => 'search_prompt',
        'contents' => $searchPrompt,
      ],
      [
        'name' => 'output_format',
        'contents' => $outputFormat,
      ]
    ];
    if ($negativePrompt) {
      $guzzleOptions['name'] = 'negative_prompt';
      $guzzleOptions['contents'] = $negativePrompt;
    }

    return $this->makeRequest("v2beta/stable-image/edit/search-and-replace", [], 'POST', "", $guzzleOptions)->getContents();
  }

  /**
   * Remove background from image.
   *
   * @param string $binary
   *   The binary.
   * @param string $outputFormat
   *   The return format.
   *
   * @return string
   *   Binary.
   */
  public function removeBackground($binary, $outputFormat = 'png') {
    // Check so return format is one of png or webp.
    if (!in_array($outputFormat, ['png', 'webp'])) {
      throw new AiInterpolatorRequestErrorException('The return format has to be one of png or webp.');
    }
    //  The image has to have a height and width dividable by 16.
    $resolution = getimagesizefromstring($binary);
    $width = $resolution[0];
    $height = $resolution[1];
    if ($width % 16 || $height % 16) {
      throw new AiInterpolatorRequestErrorException('The image has to have a height and width dividable by 16.');
    }
    // The total amount of pixels can't be more than 4194304 pixels.
    if (($width * $height) > 4194304) {
      throw new AiInterpolatorRequestErrorException('The image has to have a total amount of pixels more than 4194304 (2048x2048).');
    }
    $guzzleOptions['headers']['accept'] = 'image/*';
    $multipart[] = [
      'name' => 'image',
      'contents' => $binary,
      'filename' => 'image.' . $outputFormat,
    ];
    $multipart[] = [
      'name' => 'output_format',
      'contents' => $outputFormat,
    ];
    $guzzleOptions['multipart'] = $multipart;

    return $this->makeRequest("v2beta/stable-image/edit/remove-background", [], 'POST', "", $guzzleOptions)->getContents();
  }

  /**
   * Image to video.
   *
   * @param string $binary
   *   The binary.
   * @param float $cfgScale
   *   The CFG scale.
   * @param int $motionBucket
   *   The motion bucket.
   *
   * @return array
   *   The response as array.
   */
  public function imageToVideo($binary, $cfgScale = 1.8, $motionBucket = 127) {
    $guzzleOptions['multipart'] = [
      [
        'name' => 'image',
        'contents' => $binary,
        'filename' => 'image.png',
      ],
      [
        'name' => 'cfg_scale',
        'contents' => $cfgScale,
      ],
      [
        'name' => 'motion_bucket',
        'contents' => $motionBucket,
      ],
    ];

    return json_decode($this->makeRequest("v2beta/image-to-video", [], 'POST', "", $guzzleOptions)->getContents(), TRUE);
  }

  /**
   * Poll video.
   *
   * @param int $id
   *   The video id.
   *
   * @return string|null
   *   The binary or null if its still generating.
   */
  public function pollVideo($id) {
    return $this->makeRequest("v2beta/image-to-video/result/$id", [], 'GET');
  }

  /**
   * Image to image augmentation.
   *
   * @param string $prompt
   *   The prompt to use.
   * @param string $binary
   *   The image binary to use.
   * @param string $style_preset
   *   The style preset.
   * @param string $engine_id
   *   Dream Studios Engine.
   * @param int $steps
   *   The amount of steps.
   * @param array $options
   *   Other options to add.
   *
   * @return string
   *   Binary.
   */
  public function augmentImage($prompt, $binary, $style_preset = 'photographic', $engine_id = 'stable-diffusion-xl-beta-v2-2-2', $steps = 150, $options = []) {
    $multipart = [];
    foreach ($options as $key => $value) {
      $multipart[] = [
        'name' => $key,
        'contents' => $value,
      ];
    }
    $data = $options;
    $multipart[] = [
      'name' => 'init_image',
      'contents' => $binary,
    ];
    $multipart[] = [
      'name' => 'style_preset',
      'contents' => $style_preset,
    ];
    $multipart[] = [
      'name' => 'steps',
      'contents' => $steps,
    ];
    $multipart[] = [
      'name' => 'text_prompts[0][text]',
      'contents' => $prompt,
    ];
    $multipart[] = [
      'name' => 'text_prompts[0][weight]',
      'contents' => 1,
    ];
    $data['text_prompts'][] = [
      'text' => $prompt,
      'weight' => 1,
    ];
    $guzzleOptions['multipart'] = $multipart;
    $guzzleOptions['headers']['accept'] = 'image/png';
    $result = $this->makeRequest("v2beta/generation/$engine_id/image-to-image", [], 'POST', NULL, $guzzleOptions);
    return $result;
  }

  /**
   * Make DreamStudio call.
   *
   * @param string $path
   *   The path.
   * @param array $query_string
   *   The query string.
   * @param string $method
   *   The method.
   * @param string $body
   *   Data to attach if POST/PUT/PATCH.
   * @param array $options
   *   Extra headers.
   *
   * @return string|object
   *   The return response.
   */
  protected function makeRequest($path, array $query_string = [], $method = 'GET', $body = '', array $options = []) {
    // Check so the api key exists.
    if (!$this->apiKey) {
      throw new AiInterpolatorRequestErrorException('The Dreamstudio API key has not been set.');
    }
    // We can wait some.
    $options['connect_timeout'] = 30;
    $options['read_timeout'] = 30;
    // Don't let Guzzle die, just forward body and status.
    $options['http_errors'] = FALSE;
    // Headers.
    $options['headers']['Authorization'] = 'Bearer ' . $this->apiKey;

    if ($body) {
      $options['body'] = $body;
    }

    $new_url = $this->basePath . $path;
    $new_url .= count($query_string) ? '?' . http_build_query($query_string) : '';

    $res = $this->client->request($method, $new_url, $options);

    // Get the response code.
    $code = $res->getStatusCode();
    // Fail on 4xx and 5xx.
    if (in_array(substr($code, 0, 1), ['4', '5'])) {
      throw new AiInterpolatorRequestErrorException('The Dreamstudio API returned an error: ' . $res->getBody() . ' with code ' . $code);
    }

    return $res->getBody();
  }

}
