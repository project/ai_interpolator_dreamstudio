<?php

namespace Drupal\ai_interpolator_dreamstudio\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_dreamstudio\DreamStudio;
use Drupal\Component\Utility\Bytes;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Utility\Token;
use Drupal\file\FileRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The rules for an image field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_dreamstudio_image",
 *   title = @Translation("DreamStudio Image"),
 *   field_rule = "image",
 *   target = "file"
 * )
 */
class DreamStudioImage extends AiInterpolatorFieldRule implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'DreamStudio Image Generator';

  /**
   * The entity type manager.
   */
  public EntityTypeManagerInterface $entityManager;

  /**
   * The DreamStudio requester.
   */
  public DreamStudio $dreamStudio;

  /**
   * The File System interface.
   */
  public FileSystemInterface $fileSystem;

  /**
   * The File Repo.
   */
  public FileRepositoryInterface $fileRepo;

  /**
   * The token system to replace and generate paths.
   */
  public Token $token;

  /**
   * The current user.
   */
  public AccountProxyInterface $currentUser;

  /**
   * The logger channel.
   */
  public LoggerChannelFactoryInterface $loggerChannel;

  /**
   * Construct an image field.
   *
   * @param array $configuration
   *   Inherited configuration.
   * @param string $plugin_id
   *   Inherited plugin id.
   * @param mixed $plugin_definition
   *   Inherited plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityManager
   *   The entity type manager.
   * @param \Drupal\ai_interpolator_dreamstudio\DreamStudio $dreamStudio
   *   The DreamStudio requester.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The File system interface.
   * @param \Drupal\file\FileRepositoryInterface $fileRepo
   *   The File repo.
   * @param \Drupal\Core\Utility\Token $token
   *   The token system.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannel
   *   The logger channel interface.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityManager,
    DreamStudio $dreamStudio,
    FileSystemInterface $fileSystem,
    FileRepositoryInterface $fileRepo,
    Token $token,
    AccountProxyInterface $currentUser,
    LoggerChannelFactoryInterface $loggerChannel,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entityManager;
    $this->dreamStudio = $dreamStudio;
    $this->fileSystem = $fileSystem;
    $this->fileRepo = $fileRepo;
    $this->token = $token;
    $this->currentUser = $currentUser;
    $this->loggerChannel = $loggerChannel;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('ai_interpolator_dreamstudio.api'),
      $container->get('file_system'),
      $container->get('file.repository'),
      $container->get('token'),
      $container->get('current_user'),
      $container->get('logger.factory'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function helpText() {
    return $this->t("Generate images based on a string field. The prompt is just used to add support words to the context that has to be a plain text.");
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "{{ context }}, 50mm portrait photography, hard rim lighting photography-beta -ar 2:3 -beta -upbeta -upbeta";
  }

  /**
   * {@inheritDoc}
   */
  public function extraFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $form['interpolator_image_dreamstudio_description'] = [
      '#markup' => '<strong>The prompt will be what is used to generate the image(s).</strong>',
    ];

    $cardinality = $fieldDefinition->getFieldStorageDefinition()->getCardinality();
    $defaultGeneration = $cardinality < 0 || $cardinality > 10 ? 10 : $cardinality;
    $textAmount = $cardinality == -1 ? 'unlimited' : $cardinality;

    $form['interpolator_image_dreamstudio_amount'] = [
      '#type' => 'number',
      '#title' => 'Generation Amount',
      '#description' => $this->t('Amount of images to generate. Generation costs money, so make sure to set this correct. You can set %amount image(s).', [
        '%amount' => $textAmount,
      ]),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_image_dreamstudio_amount', $defaultGeneration),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function extraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $response = $this->dreamStudio->engines();
    $engines = [];
    foreach ($response as $result) {
      $engines[$result['id']] = $result['name'];
    }

    $response = $this->dreamStudio->engines();
    $engines = [];
    foreach ($response as $result) {
      $engines[$result['id']] = $result['name'];
    }

    $form['interpolator_dreamstudio_engine'] = [
      '#type' => 'select',
      '#title' => 'DreamStudio Engine',
      '#description' => $this->t('Choose the engine you want to use here.'),
      '#options' => $engines,
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_dreamstudio_engine', 'stable-diffusion-v1-5'),
      '#weight' => 24,
    ];

    $form['interpolator_dreamstudio_style_preset'] = [
      '#type' => 'select',
      '#title' => 'DreamStudio Style Preset',
      '#description' => $this->t('Choose the style preset you want to use here.'),
      '#options' => [
        '3d-model' => '3D Model',
        'analog-film' => 'Analog Film',
        'anime' => 'Anime',
        'cinematic' => 'Cinematic',
        'comic-book' => 'Comic Book',
        'digital-art' => 'Digital Art',
        'enhance' => 'Enhance',
        'fantasy-art' => 'Fantasy Art',
        'isometric' => 'Isometric',
        'line-art' => 'Line Art',
        'low-poly' => 'Low Poly',
        'modeling-compound' => 'Modeling Compound',
        'neon-punk' => 'Neon Punk',
        'origami' => 'Origami',
        'photographic' => 'Photographic',
        'tile-texture' => 'Tile Texture',
      ],
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_dreamstudio_style_preset', 'photographic'),
      '#weight' => 24,
    ];

    $form['interpolator_dreamstudio_width'] = [
      '#type' => 'number',
      '#title' => 'DreamStudio Width',
      '#description' => $this->t('Choose the width of the produced image. Must be in incrementes of 64.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_dreamstudio_width', 512),
      '#weight' => 24,
    ];

    $form['interpolator_dreamstudio_height'] = [
      '#type' => 'number',
      '#title' => 'DreamStudio Height',
      '#description' => $this->t('Choose the height of the produced image. Must be in incrementes of 64.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_dreamstudio_height', 512),
      '#weight' => 24,
    ];

    $form['interpolator_dreamstudio_steps'] = [
      '#type' => 'number',
      '#title' => 'DreamStudio Steps',
      '#description' => $this->t('Number of diffusion steps to run. Between 10-150.'),
      '#max' => 150,
      '#min' => 10,
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_dreamstudio_steps', 50),
      '#weight' => 24,
    ];

    $form['interpolator_dreamstudio_cfg_scale'] = [
      '#type' => 'number',
      '#title' => 'DreamStudio CfgScale',
      '#description' => $this->t('How strictly the diffusion adhears to the prompt. Between 0-35.'),
      '#max' => 35,
      '#min' => 0,
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_dreamstudio_cfg_scale', 7),
      '#weight' => 24,
    ];

    $form['interpolator_dreamstudio_seed'] = [
      '#type' => 'number',
      '#title' => 'DreamStudio Seed',
      '#description' => $this->t('Random noise seed. 0 is random. Up to 4294967295.'),
      '#max' => 4294967295,
      '#min' => 0,
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_dreamstudio_samples', 0),
      '#weight' => 24,
    ];

    $form['interpolator_dreamstudio_clip_guidence_preset'] = [
      '#type' => 'select',
      '#title' => 'DreamStudio Clip Guidence',
      '#options' => [
        'FAST_BLUE' => 'FAST_BLUE',
        'FAST_GREEN' => 'FAST_GREEN',
        'NONE' => 'NONE',
        'SIMPLE' => 'SIMPLE',
        'SLOW' => 'SLOW',
        'SLOWER' => 'SLOWER',
        'SLOWEST' => 'SLOWEST',
      ],
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_dreamstudio_clip_guidence_preset', 'NONE'),
      '#weight' => 24,
    ];

    $form['interpolator_dreamstudio_sampler'] = [
      '#type' => 'select',
      '#title' => 'DreamStudio Sampler',
      '#description' => $this->t('Which sampler to use for the diffusion process. If this value is omitted StabilityAI automatically select an appropriate sampler for you.'),
      '#options' => [
        '' => '-- None --',
        'DDIM' => 'DDIM',
        'DDPM' => 'DDPM',
        'K_DPMPP_2M' => 'K_DPMPP_2M',
        'K_DPMPP_2S_ANCESTRAL' => 'K_DPMPP_2S_ANCESTRAL',
        'K_DPM_2' => 'K_DPM_2',
        'K_DPM_2_ANCESTRAL' => 'K_DPM_2_ANCESTRAL',
        'K_EULER' => 'K_EULER',
        'K_EULER_ANCESTRAL' => 'K_EULER_ANCESTRAL',
        'K_HEUN' => 'K_HEUN',
        'K_LMS' => 'K_LMS',
      ],
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_dreamstudio_sampler', ''),
      '#weight' => 24,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $prompts = parent::generate($entity, $fieldDefinition, $interpolatorConfig);
    $amount = $interpolatorConfig['image_dreamstudio_amount'] ?? 1;
    $engine = $interpolatorConfig['dreamstudio_image'] ?? 'stable-diffusion-v1-5';
    $style = $interpolatorConfig['dreamstudio_style_preset'] ?? 'photographic';
    $width = $interpolatorConfig['dreamstudio_width'] ?? 512;
    $height = $interpolatorConfig['dreamstudio_height'] ?? 512;
    $steps = $interpolatorConfig['dreamstudio_steps'] ?? 50;
    $options['cfg_scale'] = (int) $interpolatorConfig['dreamstudio_cfg_scale'] ?? 7;
    $options['seed'] = (int) $interpolatorConfig['dreamstudio_seed'] ?? 0;
    $options['clip_guidence_preset'] = $interpolatorConfig['dreamstudio_clip_guidence_preset'] ?? 'NONE';
    if (!empty($interpolatorConfig['dreamstudio_sampler'])) {
      $options['sampler'] = $interpolatorConfig['dreamstudio_sampler'];
    }
    $total = [];
    // Add to get functional output.
    foreach ($prompts as $i => $prompt) {
      try {
        $values = [];
        for ($i = 0; $i < $amount; $i++) {
          $values[] = $this->dreamStudio->generateImage($prompt, $style, $engine, $width, $height, $steps, $options);
        }
        $total = array_merge_recursive($total, $values);
      }
      catch (\Exception $e) {

      }
    }
    return $total;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    $config = $fieldDefinition->getConfig($entity->bundle())->getSettings();
    // Validate file size if needed.
    if (!empty($config['max_filesize']) && strlen($value) > Bytes::toNumber($config['max_filesize'])) {
      $this->loggerChannel->get('ai_interpolator')->warning('The DreamStudio image being created was larger then max filesize. It was %size bytes', [
        '%size' => strlen($value),
      ]);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    $config = $fieldDefinition->getConfig($entity->bundle())->getSettings();
    // Transform string to boolean.
    $fileEntities = [];

    // Successful counter, to only download as many as max.
    $successFul = 0;
    foreach ($values as $value) {
      // Everything validated, then we prepare the file path to save to.
      $filePath = $this->token->replace($config['uri_scheme'] . '://' . rtrim($config['file_directory'], '/')) . '/DreamStudio.jpg';
      // Create file entity from string.
      $file = $this->generateFileFromString($value, $filePath);
      // If we can save, we attach it.
      if ($file) {
        // Get resolution.
        $resolution = getimagesize($file->uri->value);
        // Add to the entities list.
        $fileEntities[] = [
          'target_id' => $file->id(),
          'alt' => $config['default_image']['alt'] ?? '',
          'title' => $config['default_image']['title'] ?? '',
          'width' => $resolution[0],
          'height' => $resolution[1],
        ];

        $successFul++;
        // If we have enough images, give up.
        if ($successFul == $fieldDefinition->getFieldStorageDefinition()->getCardinality()) {
          break;
        }
      }
    }

    // Then set the value.
    $entity->set($fieldDefinition->getName(), $fileEntities);
  }

  /**
   * Generate a file entity.
   *
   * @param string $binary
   *   The source binary.
   * @param string $dest
   *   The destination.
   *
   * @return \Drupal\file\FileInterface|false
   *   The file or false on failure.
   */
  private function generateFileFromString(string $binary, string $dest) {
    // File storage.
    $fileStorage = $this->entityManager->getStorage('file');
    // Calculate path.
    $fileName = basename($dest);
    $path = substr($dest, 0, -(strlen($dest) + 1));
    // Create directory if not existsing.
    $this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);
    $file = $this->fileRepo->writeData($binary, $dest, FileSystemInterface::EXISTS_RENAME);
    if ($file) {
      return $file;
    }
    return FALSE;
  }

}
