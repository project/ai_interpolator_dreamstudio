<?php

namespace Drupal\ai_interpolator_dreamstudio\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_dreamstudio\DreamStudio;
use Drupal\Component\Utility\Bytes;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Utility\Token;
use Drupal\file\FileRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The rules for upscaling an image field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_dreamstudio_upscaler",
 *   title = @Translation("DreamStudio Image Upscaler"),
 *   field_rule = "image",
 *   target = "file"
 * )
 */
class UpscaleImage extends AiInterpolatorFieldRule implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'DreamStudio Image Upscaler';

  /**
   * The entity type manager.
   */
  public EntityTypeManagerInterface $entityManager;

  /**
   * The DreamStudio requester.
   */
  public DreamStudio $dreamStudio;

  /**
   * The File System interface.
   */
  public FileSystemInterface $fileSystem;

  /**
   * The File Repo.
   */
  public FileRepositoryInterface $fileRepo;

  /**
   * The token system to replace and generate paths.
   */
  public Token $token;

  /**
   * The current user.
   */
  public AccountProxyInterface $currentUser;

  /**
   * The logger channel.
   */
  public LoggerChannelFactoryInterface $loggerChannel;

  /**
   * The messenger.
   */
  public Messenger $messenger;

  /**
   * Construct an image field.
   *
   * @param array $configuration
   *   Inherited configuration.
   * @param string $plugin_id
   *   Inherited plugin id.
   * @param mixed $plugin_definition
   *   Inherited plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityManager
   *   The entity type manager.
   * @param \Drupal\ai_interpolator_dreamstudio\DreamStudio $dreamStudio
   *   The DreamStudio requester.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The File system interface.
   * @param \Drupal\file\FileRepositoryInterface $fileRepo
   *   The File repo.
   * @param \Drupal\Core\Utility\Token $token
   *   The token system.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannel
   *   The logger channel interface.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityManager,
    DreamStudio $dreamStudio,
    FileSystemInterface $fileSystem,
    FileRepositoryInterface $fileRepo,
    Token $token,
    AccountProxyInterface $currentUser,
    LoggerChannelFactoryInterface $loggerChannel,
    Messenger $messenger,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entityManager;
    $this->dreamStudio = $dreamStudio;
    $this->fileSystem = $fileSystem;
    $this->fileRepo = $fileRepo;
    $this->token = $token;
    $this->currentUser = $currentUser;
    $this->loggerChannel = $loggerChannel;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('ai_interpolator_dreamstudio.api'),
      $container->get('file_system'),
      $container->get('file.repository'),
      $container->get('token'),
      $container->get('current_user'),
      $container->get('logger.factory'),
      $container->get('messenger'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function needsPrompt() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function helpText() {
    return $this->t("Upscale an image up to 4x in size.");
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "";
  }

  /**
   * {@inheritDoc}
   */
  public function allowedInputs() {
    return ['image'];
  }

  /**
   * {@inheritDoc}
   */
  public function extraFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {

    $form['interpolator_dreamstudio_upscaler_width'] = [
      '#type' => 'number',
      '#title' => 'Width',
      '#min' => 512,
      '#max' => 2048,
      '#description' => $this->t('Either set a width or a height to scale to. Max 2048.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_dreamstudio_upscaler_width', 2048),
    ];

    $form['interpolator_dreamstudio_upscaler_height'] = [
      '#type' => 'number',
      '#title' => 'Height',
      '#min' => 512,
      '#max' => 2048,
      '#description' => $this->t('Either set a width or a height to scale to. If both are filled, width is used.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_dreamstudio_upscaler_height', NULL),
    ];

    $form['interpolator_dreamstudio_downscale'] = [
      '#type' => 'checkbox',
      '#title' => 'Downscale to upscale',
      '#min' => 512,
      '#max' => 2048,
      '#description' => $this->t('The max height/width of the image is 1024, if in between 1024 and the value you set, should we downscale to upscale?'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_dreamstudio_downscale', NULL),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $values = [];
    foreach ($entity->{$interpolatorConfig['base_field']} as $target) {
      if ($target->entity) {
        $resolution = getimagesize($target->entity->getFileUri());
        if (empty($resolution[2])) {
          break;
        }
        $width = 0;
        $height = 0;

        $encoded = file_get_contents($target->entity->getFileUri());
        if (!empty($interpolatorConfig['dreamstudio_upscaler_width'])) {
          $width = $interpolatorConfig['dreamstudio_upscaler_width'];
          // If the resolution of the original is too big or too small, just copy and message.
          if ($resolution[0] < 512 || $resolution[0] > $width || ($resolution[0] > 1024 && empty($interpolatorConfig['dreamstudio_downscale']))) {
            $values[] = $encoded;
            $this->messenger->addWarning('The image was not possible to upscale, so we copied it.');
            continue;
          }
          // If the resolution is over 1024, downscale if wanted.
          if ($resolution[0] > 1024 && !empty($interpolatorConfig['dreamstudio_downscale'])) {
            $image = imagecreatefromstring($encoded);
            $image = imagescale($image, 1024, round(($resolution[1] / $resolution[0]) * 1024), IMG_BICUBIC);
            ob_start();
            imagepng($image);
            $encoded = ob_get_clean();
          }
        }
        else {
          $height = $interpolatorConfig['dreamstudio_upscaler_height'];
          // If the resolution of the original is too big or too small, just copy and message.
          if ($resolution[1] < 512 || $resolution[1] > $height || ($resolution[1] > 1024 && empty($interpolatorConfig['dreamstudio_downscale']))) {
            $values[] = $encoded;
            $this->messenger->addWarning('The image was not possible to upscale, so we copied it.');
            continue;
          }
          // If the resolution is over 1024, downscale if wanted.
          if ($resolution[0] > 1024 && !empty($interpolatorConfig['dreamstudio_downscale'])) {
            $image = imagecreatefromstring($encoded);
            $image = imagescale($image, round(($resolution[0] / $resolution[1]) * 1024), 1024, IMG_BICUBIC);
            ob_start();
            imagepng($image);
            $encoded = ob_get_clean();
          }
        }

        if ($encoded) {
          $return = $this->dreamStudio->upscaleImage($encoded, $width, $height);
          $values[] = $return;
        }
      }
    }
    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition)
  {
    $config = $fieldDefinition->getConfig($entity->bundle())->getSettings();
    // Validate file size if needed.
    if (!empty($config['max_filesize']) && strlen($value) > Bytes::toNumber($config['max_filesize'])) {
      $this->loggerChannel->get('ai_interpolator')->warning('The DreamStudio image being created was larger then max filesize. It was %size bytes', [
        '%size' => strlen($value),
      ]);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    $config = $fieldDefinition->getConfig($entity->bundle())->getSettings();
    // Transform string to boolean.
    $fileEntities = [];

    // Successful counter, to only download as many as max.
    $successFul = 0;
    foreach ($values as $value) {
      // Everything validated, then we prepare the file path to save to.
      $filePath = $this->token->replace($config['uri_scheme'] . '://' . rtrim($config['file_directory'], '/')) . '/DreamStudio.jpg';
      // Create file entity from string.
      $file = $this->generateFileFromString($value, $filePath);
      // If we can save, we attach it.
      if ($file) {
        // Get resolution.
        $resolution = getimagesize($file->uri->value);
        // Add to the entities list.
        $fileEntities[] = [
          'target_id' => $file->id(),
          'alt' => $config['default_image']['alt'] ?? '',
          'title' => $config['default_image']['title'] ?? '',
          'width' => $resolution[0],
          'height' => $resolution[1],
        ];

        $successFul++;
        // If we have enough images, give up.
        if ($successFul == $fieldDefinition->getFieldStorageDefinition()->getCardinality()) {
          break;
        }
      }
    }

    // Then set the value.
    $entity->set($fieldDefinition->getName(), $fileEntities);
  }

  /**
   * Generate a file entity.
   *
   * @param string $binary
   *   The source binary.
   * @param string $dest
   *   The destination.
   *
   * @return \Drupal\file\FileInterface|false
   *   The file or false on failure.
   */
  private function generateFileFromString(string $binary, string $dest) {
    // File storage.
    $fileStorage = $this->entityManager->getStorage('file');
    // Calculate path.
    $fileName = basename($dest);
    $path = substr($dest, 0, - (strlen($dest) + 1));
    // Create directory if not existsing.
    $this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);
    $file = $this->fileRepo->writeData($binary, $dest, FileSystemInterface::EXISTS_RENAME);
    if ($file) {
      return $file;
    }
    return FALSE;
  }
}
