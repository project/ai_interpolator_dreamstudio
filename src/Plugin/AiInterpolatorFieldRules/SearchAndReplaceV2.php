<?php

namespace Drupal\ai_interpolator_dreamstudio\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_dreamstudio\DreamStudio;
use Drupal\Component\Utility\Bytes;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Utility\Token;
use Drupal\file\FileRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The rules for a search and replace image field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_dreamstudio_search_and_replace",
 *   title = @Translation("DreamStudio Search And Replace (v2)"),
 *   field_rule = "image",
 *   target = "file"
 * )
 */
class SearchAndReplaceV2 extends AiInterpolatorFieldRule implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'DreamStudio Search And Replace (v2)';

  /**
   * The entity type manager.
   */
  public EntityTypeManagerInterface $entityManager;

  /**
   * The DreamStudio requester.
   */
  public DreamStudio $dreamStudio;

  /**
   * The File System interface.
   */
  public FileSystemInterface $fileSystem;

  /**
   * The File Repo.
   */
  public FileRepositoryInterface $fileRepo;

  /**
   * The token system to replace and generate paths.
   */
  public Token $token;

  /**
   * The current user.
   */
  public AccountProxyInterface $currentUser;

  /**
   * The logger channel.
   */
  public LoggerChannelFactoryInterface $loggerChannel;

  /**
   * Construct an image field.
   *
   * @param array $configuration
   *   Inherited configuration.
   * @param string $plugin_id
   *   Inherited plugin id.
   * @param mixed $plugin_definition
   *   Inherited plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityManager
   *   The entity type manager.
   * @param \Drupal\ai_interpolator_dreamstudio\DreamStudio $dreamStudio
   *   The DreamStudio requester.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The File system interface.
   * @param \Drupal\file\FileRepositoryInterface $fileRepo
   *   The File repo.
   * @param \Drupal\Core\Utility\Token $token
   *   The token system.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannel
   *   The logger channel interface.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityManager,
    DreamStudio $dreamStudio,
    FileSystemInterface $fileSystem,
    FileRepositoryInterface $fileRepo,
    Token $token,
    AccountProxyInterface $currentUser,
    LoggerChannelFactoryInterface $loggerChannel,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entityManager;
    $this->dreamStudio = $dreamStudio;
    $this->fileSystem = $fileSystem;
    $this->fileRepo = $fileRepo;
    $this->token = $token;
    $this->currentUser = $currentUser;
    $this->loggerChannel = $loggerChannel;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('ai_interpolator_dreamstudio.api'),
      $container->get('file_system'),
      $container->get('file.repository'),
      $container->get('token'),
      $container->get('current_user'),
      $container->get('logger.factory'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function needsPrompt() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function helpText() {
    return $this->t("Search and replace something specific in an image.");
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "";
  }

  /**
   * {@inheritDoc}
   */
  public function allowedInputs() {
    return ['image'];
  }

  /**
   * {@inheritDoc}
   */
  public function extraFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $form['interpolator_dreamstudio_prompt'] = [
      '#type' => 'textarea',
      '#title' => 'Prompt',
      '#attributes' => [
        'placeholder' => 'The sky was a crisp (blue:0.3) and (green:0.8)',
      ],
      '#description' => $this->t('The prompt of what you want to see in the image.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_dreamstudio_prompt', ''),
    ];

    $this->addTokenConfigurationFormField('interpolator_dreamstudio_prompt', $form, $entity, $fieldDefinition);

    $form['interpolator_dreamstudio_search_prompt'] = [
      '#type' => 'textarea',
      '#title' => 'Search Prompt',
      '#attributes' => [
        'placeholder' => 'The sky',
      ],
      '#description' => $this->t('The search prompt of what to replace.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_dreamstudio_search_prompt', ''),
    ];

    $this->addTokenConfigurationFormField('interpolator_dreamstudio_search_prompt', $form, $entity, $fieldDefinition);

    $form['interpolator_dreamstudio_negative_prompt'] = [
      '#type' => 'textarea',
      '#title' => 'Search Prompt',
      '#attributes' => [
        'placeholder' => 'The sky'
      ],
      '#description' => $this->t('The search prompt of what to replace.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_dreamstudio_search_prompt', ''),
    ];

    $this->addTokenConfigurationFormField('interpolator_dreamstudio_negative_prompt', $form, $entity, $fieldDefinition);

    $form['interpolator_dreamstudio_format'] = [
      '#type' => 'select',
      '#options' => [
        'png' => 'PNG',
        'jpg' => 'JPG',
        'webp' => 'WebP',
      ],
      '#title' => 'Output format',
      '#description' => $this->t('PNG, JPG or WebP'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_dreamstudio_format', 'png'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigValues($form, FormStateInterface $formState) {
    $values = $formState->getValues();
    // Required values.
    if (empty($values['interpolator_dreamstudio_prompt']) && empty($values['interpolator_dreamstudio_prompt_token'])) {
      $formState->setErrorByName('interpolator_dreamstudio_prompt', $this->t('You need to provide a prompt.'));
    }
    if (empty($values['interpolator_dreamstudio_search_prompt']) && empty($values['interpolator_dreamstudio_search_prompt_token'])) {
      $formState->setErrorByName('interpolator_dreamstudio_search_prompt', $this->t('You need to provide a search prompt.'));
    }
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $values = [];
    foreach ($entity->{$interpolatorConfig['base_field']} as $target) {
      if ($target->entity) {

        $encoded = file_get_contents($target->entity->getFileUri());

        if ($encoded) {
          $prompt = $this->getConfigValue('dreamstudio_prompt', $interpolatorConfig, $entity);
          $search = $this->getConfigValue('dreamstudio_search_prompt', $interpolatorConfig, $entity);
          $return = $this->dreamStudio->searchAndReplace($encoded, $prompt, $search, $interpolatorConfig['dreamstudio_negative_prompt'], $interpolatorConfig['dreamstudio_format']);
          $values[] = $return;
        }
      }
    }
    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    $config = $fieldDefinition->getConfig($entity->bundle())->getSettings();
    // Validate file size if needed.
    if (!empty($config['max_filesize']) && strlen($value) > Bytes::toNumber($config['max_filesize'])) {
      $this->loggerChannel->get('ai_interpolator')->warning('The DreamStudio image being created was larger then max filesize. It was %size bytes', [
        '%size' => strlen($value),
      ]);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    $config = $fieldDefinition->getConfig($entity->bundle())->getSettings();
    $outputFormat = $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_dreamstudio_format', 'png');
    // Transform string to boolean.
    $fileEntities = [];

    // Successful counter, to only download as many as max.
    $successFul = 0;
    foreach ($values as $value) {
      // Everything validated, then we prepare the file path to save to.
      $filePath = $this->token->replace($config['uri_scheme'] . '://' . rtrim($config['file_directory'], '/')) . '/searchreplace.' . $outputFormat;
      // Create file entity from string.
      $file = $this->generateFileFromString($value, $filePath);
      // If we can save, we attach it.
      if ($file) {
        // Get resolution.
        $resolution = getimagesize($file->uri->value);
        // Add to the entities list.
        $fileEntities[] = [
          'target_id' => $file->id(),
          'alt' => $config['default_image']['alt'] ?? '',
          'title' => $config['default_image']['title'] ?? '',
          'width' => $resolution[0],
          'height' => $resolution[1],
        ];

        $successFul++;
        // If we have enough images, give up.
        if ($successFul == $fieldDefinition->getFieldStorageDefinition()->getCardinality()) {
          break;
        }
      }
    }

    // Then set the value.
    $entity->set($fieldDefinition->getName(), $fileEntities);
  }

  /**
   * Generate a file entity.
   *
   * @param string $binary
   *   The source binary.
   * @param string $dest
   *   The destination.
   *
   * @return \Drupal\file\FileInterface|false
   *   The file or false on failure.
   */
  private function generateFileFromString(string $binary, string $dest) {
    // File storage.
    $fileStorage = $this->entityManager->getStorage('file');
    // Calculate path.
    $fileName = basename($dest);
    $path = substr($dest, 0, - (strlen($dest) + 1));
    // Create directory if not existsing.
    $this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);
    $file = $this->fileRepo->writeData($binary, $dest, FileSystemInterface::EXISTS_RENAME);
    if ($file) {
      return $file;
    }
    return FALSE;
  }
}
