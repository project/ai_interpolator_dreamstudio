<?php

namespace Drupal\ai_interpolator_dreamstudio\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator\Exceptions\AiInterpolatorRequestErrorException;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_dreamstudio\DreamStudio;
use Drupal\Component\Utility\Bytes;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Utility\Token;
use Drupal\file\FileRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The rules for image to video field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_dreamstudio_image_to_video_v2",
 *   title = @Translation("DreamStudio Image to Video (v2)"),
 *   field_rule = "file",
 *   target = "file",
 * )
 */
class ImageToVideoV2 extends AiInterpolatorFieldRule implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'DreamStudio Image to Video (v2)';

  /**
   * The entity type manager.
   */
  public EntityTypeManagerInterface $entityManager;

  /**
   * The DreamStudio requester.
   */
  public DreamStudio $dreamStudio;

  /**
   * The File System interface.
   */
  public FileSystemInterface $fileSystem;

  /**
   * The File Repo.
   */
  public FileRepositoryInterface $fileRepo;

  /**
   * The token system to replace and generate paths.
   */
  public Token $token;

  /**
   * The current user.
   */
  public AccountProxyInterface $currentUser;

  /**
   * The logger channel.
   */
  public LoggerChannelFactoryInterface $loggerChannel;

  /**
   * The messenger.
   */
  public Messenger $messenger;

  /**
   * Construct an image field.
   *
   * @param array $configuration
   *   Inherited configuration.
   * @param string $plugin_id
   *   Inherited plugin id.
   * @param mixed $plugin_definition
   *   Inherited plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityManager
   *   The entity type manager.
   * @param \Drupal\ai_interpolator_dreamstudio\DreamStudio $dreamStudio
   *   The DreamStudio requester.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The File system interface.
   * @param \Drupal\file\FileRepositoryInterface $fileRepo
   *   The File repo.
   * @param \Drupal\Core\Utility\Token $token
   *   The token system.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannel
   *   The logger channel interface.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityManager,
    DreamStudio $dreamStudio,
    FileSystemInterface $fileSystem,
    FileRepositoryInterface $fileRepo,
    Token $token,
    AccountProxyInterface $currentUser,
    LoggerChannelFactoryInterface $loggerChannel,
    Messenger $messenger,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entityManager;
    $this->dreamStudio = $dreamStudio;
    $this->fileSystem = $fileSystem;
    $this->fileRepo = $fileRepo;
    $this->token = $token;
    $this->currentUser = $currentUser;
    $this->loggerChannel = $loggerChannel;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('ai_interpolator_dreamstudio.api'),
      $container->get('file_system'),
      $container->get('file.repository'),
      $container->get('token'),
      $container->get('current_user'),
      $container->get('logger.factory'),
      $container->get('messenger'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function needsPrompt() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function helpText() {
    return $this->t("Make an image move.");
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "";
  }

  /**
   * {@inheritDoc}
   */
  public function allowedInputs() {
    return ['image'];
  }

  /**
   * {@inheritDoc}
   */
  public function extraFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {

    $form['interpolator_dreamstudio_cfg_scale'] = [
      '#type' => 'number',
      '#min' => 0,
      '#max' => 10,
      '#step' => '0.1',
      '#title' => 'Classifier Guidance Scale',
      '#description' => $this->t('How strongly the video sticks to the original image. Use lower values to allow the model more freedom to make changes and higher values to correct motion distortions.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_dreamstudio_cfg_scale', 1.8),
    ];

    $form['interpolator_dreamstudio_motion_bucket'] = [
      '#type' => 'number',
      '#min' => 0,
      '#max' => 255,
      '#title' => 'Motion Bucket',
      '#description' => $this->t('The motion bucket is a parameter that controls the amount of motion in the video. Use lower values to create a more static video and higher values to create a more dynamic video.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_dreamstudio_motion_bucket', 127),
    ];

    $form['interpolator_dreamstudio_timeout'] = [
      '#type' => 'number',
      '#min' => 0,
      '#max' => 1200,
      '#title' => 'Timeout',
      '#description' => $this->t('The maximum time in seconds to wait for the video to be created.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_dreamstudio_timeout', 120),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $values = [];
    foreach ($entity->{$interpolatorConfig['base_field']} as $target) {
      if ($target->entity) {

        $encoded = file_get_contents($target->entity->getFileUri());

        if ($encoded) {
          $return = $this->dreamStudio->imageToVideo($encoded, $interpolatorConfig['dreamstudio_cfg_scale'], $interpolatorConfig['dreamstudio_motion_bucket']);

          if (isset($return['id'])) {
            $values[] = $this->pollVideo($return['id'], $interpolatorConfig['dreamstudio_timeout'], 0);
          }
        }
      }
    }
    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    $config = $fieldDefinition->getConfig($entity->bundle())->getSettings();
    // Validate file size if needed.
    if (!empty($config['max_filesize']) && strlen($value) > Bytes::toNumber($config['max_filesize'])) {
      $this->loggerChannel->get('ai_interpolator')->warning('The DreamStudio image being created was larger then max filesize. It was %size bytes', [
        '%size' => strlen($value),
      ]);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    $config = $fieldDefinition->getConfig($entity->bundle())->getSettings();
    // Transform string to boolean.
    $fileEntities = [];

    // Successful counter, to only download as many as max.
    $successFul = 0;
    foreach ($values as $value) {
      // Everything validated, then we prepare the file path to save to.
      $filePath = $this->token->replace($config['uri_scheme'] . '://' . rtrim($config['file_directory'], '/')) . '/ImageVideo.mp4';
      // Create file entity from string.
      $file = $this->generateFileFromString($value, $filePath);
      // If we can save, we attach it.
      if ($file) {
        // Add to the entities list.
        $fileEntities[] = [
          'target_id' => $file->id(),
        ];

        $successFul++;
        // If we have enough images, give up.
        if ($successFul == $fieldDefinition->getFieldStorageDefinition()->getCardinality()) {
          break;
        }
      }
    }

    // Then set the value.
    $entity->set($fieldDefinition->getName(), $fileEntities);
  }

  /**
   * Poll the video until its done or timeout.
   *
   * @param string $id
   *   The video id.
   * @param int $timeout
   *   The timeout.
   * @param int $counter
   *    The current time used.
   *
   * @return string
   *   The video binary.
   */
  private function pollVideo(string $id, int $timeout, int $counter) {
    $time = time();
    $video = $this->dreamStudio->pollVideo($id);
    $json = json_decode($video, TRUE);
    if ($json && $json['status'] == 'in-progress') {
      sleep(5);
      $newTime = time() - $time + $counter;
      return $this->pollVideo($id, $timeout, $newTime);
    }
    else if ($video) {
      return $video;
    }
    if ($counter > $timeout) {
      throw new AiInterpolatorRequestErrorException('The video creation timed out.');
    }
  }

  /**
   * Generate a file entity.
   *
   * @param string $binary
   *   The source binary.
   * @param string $dest
   *   The destination.
   *
   * @return \Drupal\file\FileInterface|false
   *   The file or false on failure.
   */
  private function generateFileFromString(string $binary, string $dest) {
    // File storage.
    $fileStorage = $this->entityManager->getStorage('file');
    // Calculate path.
    $fileName = basename($dest);
    $path = substr($dest, 0, - (strlen($dest) + 1));
    // Create directory if not existsing.
    $this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);
    $file = $this->fileRepo->writeData($binary, $dest, FileSystemInterface::EXISTS_RENAME);
    if ($file) {
      return $file;
    }
    return FALSE;
  }
}
