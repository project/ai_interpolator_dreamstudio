# AI Interpolator OpenAI (GPT)
## What is the AI Interpolator OpenAI (GPT) module?
This module adds the possibility to generate images from sub field. It's a submodule to the AI Interpolator module.

It currently exposes field plugins for the following field types:
* Image

### How to install the AI Interpolator module
1. Get the code like any other module.
2. Install the module.
3. Setup the DreamStudio API key here: /admin/config/dreamstudio/settings

### How to use the AI Interpolator module
1. Create a textfield used to generate the image.
2. Create an image field.
3. During field config enable it by checking `Enable AI Interpolator`
4. A sub form will open up where you can choose the Interpolator rule to use based on your sub-module.
5. Follow the instructions of the forms.
6. Create a content with the field and fill out the base field used for generation.
7. Your image should should be autopopulated.